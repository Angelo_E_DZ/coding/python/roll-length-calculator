from math import acos

total = 0
pi = round(2*acos(0.0))

innerPerimeter = float(input("Inner circle perimeter:"))
outerPerimeter = float(input("Outer circle perimeter:"))
circlesBetween = int(input("Total number of circles:"))
circlesBetween -= 2

smallRadius = (innerPerimeter/2) / pi
bigRadius = (outerPerimeter/2) / pi

diff = bigRadius - smallRadius
increments = diff/circlesBetween

for x in range(1, circlesBetween):
	total += (smallRadius + (increments * x)) * 2 * pi

print(total+innerPerimeter+outerPerimeter)